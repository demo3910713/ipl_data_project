const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const matchesPath = "../data/matches.csv";
const deliveriesPath = "../data/deliveries.csv";

function strikeRateOfThePlayerEachSeason(matchesPath, deliveriesPath, batsman) {
  try {
    csv(matchesPath).then((matchesData) => {
      csv(deliveriesPath).then((deliveriesData) => {
        let strikeRateOftheBatsman = {};
        for (const match of matchesData) {
          if (!strikeRateOftheBatsman[match.season]) {
            strikeRateOftheBatsman[match.season] = { balls: 0, runs: 0 };
          } else {
            for (const deliveries of deliveriesData) {
              if (match.id == deliveries.match_id) {
                if (deliveries.batsman == batsman) {
                  strikeRateOftheBatsman[match.season]["balls"] += 1;
                  strikeRateOftheBatsman[match.season]["runs"] += Number(
                    deliveries.batsman_runs
                  );
                }
              }
            }
          }
        }
        
        let result = {};
        for (const key in strikeRateOftheBatsman) {
          result[key] = Math.round(
            (strikeRateOftheBatsman[key].runs /
              strikeRateOftheBatsman[key].balls) *
              100
          );
        }

        fs.writeFile(
          "../public/output/strikeRateOfThePlayerEachSeason.json",
          JSON.stringify(result),
          (err) => {
            if (err) {
              console.log(err);
            } else {
              console.log("Data updated Successfully... ");
            }
          }
        );
      });
    });
  } catch (error) {
    console.log(error);
  }
}

strikeRateOfThePlayerEachSeason(matchesPath, deliveriesPath, "SK Raina");
