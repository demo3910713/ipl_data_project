const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const matchespath = "../data/matches.csv";
const deliveriesPath = "../data/deliveries.csv";

function matchesWonPerYear(matchespath, deliveriesData) {
  try {
    csv(matchespath).then((matchesData) => {
      csv(deliveriesPath).then((deliveriesData) => {
        let extraRunnersConcededperTeam = {};
        for (let index = 0; index < matchesData.length; index++) {
          const element = matchesData[index];
          if (element.season == 2016) {
            const id = element.id;
            for (let index = 0; index < deliveriesData.length; index++) {
              const element = deliveriesData[index];
              if (id === element.match_id) {
                if (!extraRunnersConcededperTeam[element.bowling_team]) {
                  extraRunnersConcededperTeam[element.bowling_team] = 0;
                }
                extraRunnersConcededperTeam[element.bowling_team] += Number(
                  element.extra_runs
                );
              }
            }
          }
        }

        // export the output into the json file
        fs.writeFile(
          "../public/output/extraRunsConcededByTeamIn2016.json",
          JSON.stringify(extraRunnersConcededperTeam),
          (err) => {
            if (err) {
              console.log(err);
            } else {
              console.log("Data updated Successfully... ");
            }
          }
        );
      });
    });
  } catch (error) {
    console.log(error);
  }
}

matchesWonPerYear(matchespath, deliveriesPath);
