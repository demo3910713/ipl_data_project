const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const deliveriesPath = "../data/deliveries.csv";

function bestEconomyInSuperOver(deliveriesPath) {
  try {
  csv(deliveriesPath).then((deliveriesData) => {
    // store  the total runs
    let runs = {};
    // store the balls
    let balls = {};
    for (const data of deliveriesData) {
      if (data.is_super_over > 0) {
        if (!runs[data.bowler]) {
          runs[data.bowler] = 0;
          balls[data.bowler] = 0;
        }
        runs[data.bowler] += Number(data.total_runs);
        balls[data.bowler] += 1;
      }
    }

    // calculate the economy and the store the variable
    let economy = [];
    for (const key in runs) {
      let temp = {};
      temp[key] = runs[key] / (balls[key] / 6);
      economy.push(temp);
    }

    // sorting of the economy
    for (let index = 0; index < economy.length; index++) {
      for (let index1 = index + 1; index1 < economy.length; index1++) {
        const value1 = Object.values(economy[index])[0];
        const value2 = Object.values(economy[index1])[0];
        if (value1 > value2) {
          const temp = economy[index];
          economy[index] = economy[index1];
          economy[index1] = temp;
        }
      }
    }

    // store the data in json file
    fs.writeFile("../public/output/bestEconomyBowlerInSuperOvers.json",
      JSON.stringify(economy[0]),
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Data updated Successfully... ");
        }
      }
    );
  });
} catch (error) {
    console.log(error)
}

}
bestEconomyInSuperOver(deliveriesPath);
