const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const matchesPath = "../data/matches.csv";

function noOfTimesWonTossAndMatch(matchesPath) {
  try {
  csv(matchesPath).then((matchesData) => {
    let result = {};
    for (let index = 0; index < matchesData.length; index++) {
      const element = matchesData[index];
      if (element.toss_winner == element.winner) {
        if (!result[element.winner]) {
          result[element.winner] = 0;
        }
        result[element.winner] += 1;
      }
    }
    
    fs.writeFile(
      "../public/output/noOfTimesWonTossAndMatch.json",
      JSON.stringify(result),
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Data updated Successfully... ");
        }
      }
    );
  });
} catch (error) {
 console.log(error)   
}
}

noOfTimesWonTossAndMatch(matchesPath);
